package ua.epam.hwTwelve;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class MainTest {
    @Autowired
    private MessageSender sender;

    @Test
    public void testApplication() throws InterruptedException {
        Thread.sleep(1000);
        Map<UUID, RequestMessage> requestMessageMap = new HashMap<>();
        for (RequestMessage requestMessage : sender.getSENT()) {
            if (requestMessageMap.put(requestMessage.getId(), requestMessage) != null) {
                throw new IllegalStateException("Duplicate key");
            }
        }

        Map<UUID, ResponseMessage> responseMessageMap = new HashMap<>();
        for (ResponseMessage message : sender.getRECEIVED()) {
            if (responseMessageMap.put(message.getId(), message) != null) {
                throw new IllegalStateException("Duplicate key");
            }
        }

        requestMessageMap.forEach((key, requestMessage) -> {
            ResponseMessage responseMessage = responseMessageMap.get(key);
            assertEquals(requestMessage.getFirstAddendum() + requestMessage.getSecondAddendum(),
                    responseMessage.getSum());
        });
    }
}
