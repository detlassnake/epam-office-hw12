package ua.epam.hwTwelve;

import java.io.Serializable;
import java.util.UUID;

public class RequestMessage implements Serializable {
    private final UUID id = UUID.randomUUID();
    private int firstAddendum;
    private int secondAddendum;

    public RequestMessage() {
    }

    public RequestMessage(int firstAddendum, int secondAddendum) {
        this.firstAddendum = firstAddendum;
        this.secondAddendum = secondAddendum;
    }

    public UUID getId() {
        return id;
    }

    public int getFirstAddendum() {
        return firstAddendum;
    }

    public void setFirstAddendum(int firstAddendum) {
        this.firstAddendum = firstAddendum;
    }

    public int getSecondAddendum() {
        return secondAddendum;
    }

    public void setSecondAddendum(int secondAddendum) {
        this.secondAddendum = secondAddendum;
    }
}
