package ua.epam.hwTwelve;

import java.util.UUID;

public class ResponseMessage {
    private UUID id;
    private int sum;

    public ResponseMessage() {
    }

    public ResponseMessage(UUID id, int sum) {
        this.id = id;
        this.sum = sum;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }
}
