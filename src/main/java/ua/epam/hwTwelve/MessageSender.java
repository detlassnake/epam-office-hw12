package ua.epam.hwTwelve;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static ua.epam.hwTwelve.ActiveMQConfig.REQUEST_CHANNEL;
import static ua.epam.hwTwelve.ActiveMQConfig.RESPONSE_CHANNEL;

@Component
public class MessageSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageSender.class);
    private final List<RequestMessage> SENT = new ArrayList<>();
    private final List<ResponseMessage> RECEIVED = new ArrayList<>();
    private JmsTemplate jmsTemplate;

    @Autowired
    public MessageSender(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @JmsListener(destination = RESPONSE_CHANNEL)
    public void receive(@Payload ResponseMessage responseMessage) {
        RECEIVED.add(responseMessage);
        LOGGER.info("Response received: id = " + responseMessage.getId() + ", sum = " + responseMessage.getSum());
    }

    private void send(RequestMessage requestMessage) {
        LOGGER.info("Request sent: id = " + requestMessage.getId() + "; "
                + requestMessage.getFirstAddendum() + " + " + requestMessage.getSecondAddendum());
        jmsTemplate.convertAndSend(REQUEST_CHANNEL, requestMessage);
        SENT.add(requestMessage);
    }

    public void send(Collection<RequestMessage> requestMessages) {
        for (RequestMessage requestMessage : requestMessages) {
            send(requestMessage);
        }

    }

    public List<RequestMessage> getSENT() {
        return SENT;
    }

    public List<ResponseMessage> getRECEIVED() {
        return RECEIVED;
    }
}
