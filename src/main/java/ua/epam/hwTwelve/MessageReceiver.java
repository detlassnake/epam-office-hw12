package ua.epam.hwTwelve;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static ua.epam.hwTwelve.ActiveMQConfig.REQUEST_CHANNEL;
import static ua.epam.hwTwelve.ActiveMQConfig.RESPONSE_CHANNEL;

@Component
public class MessageReceiver {
    private int counter = 0;
    private final Map<UUID, RequestMessage> MESSAGES = new HashMap<>();
    private JmsTemplate jmsTemplate;

    @Autowired
    public MessageReceiver(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    @JmsListener(destination = REQUEST_CHANNEL)
    public void receiveMessage(@Payload RequestMessage requestMessage) {
        if (MESSAGES.put(requestMessage.getId(), requestMessage) == null) {
            counter++;
            if (counter >= 10) {
                sendResponse();
                counter = 0;
                MESSAGES.clear();
            }
        }
    }

    public void sendResponse() {
        for (Map.Entry<UUID, RequestMessage> entry : MESSAGES.entrySet()) {
            UUID id = entry.getKey();
            RequestMessage message = entry.getValue();
            jmsTemplate.convertAndSend(RESPONSE_CHANNEL,
                    new ResponseMessage(id,
                            message.getFirstAddendum() + message.getSecondAddendum()));
        }
    }
}
