package ua.epam.hwTwelve;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class Main implements ApplicationRunner {
    private MessageSender messageSender;

    @Autowired
    public Main(MessageSender messageSender) {
        this.messageSender = messageSender;
    }

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        List<RequestMessage> messages = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            RequestMessage requestMessage = new RequestMessage(i, i + 1);
            messages.add(requestMessage);
        }
        messageSender.send(messages);
    }
}
